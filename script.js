class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  set name(name) {
    this._name = name;
  }
  set age(age) {
    this._age = age;
  }
  set salary(salary) {
    this._salary = salary;
  }

  get name() {
    return this._name;
  }
  get age() {
    return this._age;
  }
  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  set lang(lang) {
    this._lang = lang;
  }
  get lang() {
    return this._lang;
  }

  
  get salery() {
    return this._salary * 3;
  }
}

const c1 = new Employee("yana", 5, 1232);
console.log(c1);

const program = new Programmer("Olia", 10, 1234, ["JS", "HTML"]);
const program2 = new Programmer("Alex", 40, 3333, ["JS", "HTML", "Java"]);
const program3 = new Programmer("Vika", 25, 1333, [ "Python", "HTML","Java","C++"]);

console.log("Programer1: ", program);
console.log("Programer2: ", program2);
console.log("Programer3: ", program3);
